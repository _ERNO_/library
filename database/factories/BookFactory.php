<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'picture' => $this->getPhoto(rand(1, 17)),
            'author' => $this->faker->userName(),
            'title' => $this->faker->word(),
            'genre_id' => rand(1,4),
        ];
    }

    private function getPhoto(int $number = 1):string
    {
        $path = storage_path() . "/books/" . $number . ".jpg";
        $image_name = md5($path) . ".jpg";
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('photos/' . $image_name, $resize->__toString());
        return 'photos/' . $image_name;
    }
}
