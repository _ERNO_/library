<?php

namespace Database\Factories;

use App\Models\Reader;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReaderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'book' => $this->faker->word(),
            'status' => $this->faker->unique()->randomElement(Reader::STATUSES),
            'data' => $this->faker->date(),
        ];
    }
}
