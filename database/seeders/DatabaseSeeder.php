<?php

namespace Database\Seeders;

use App\Models\Reader;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $readers = Reader::factory()->count(3)->create();
        $users = User::factory()->count(5)->create();
        foreach ($users as $user)
        {
            $user->readers()->attach($readers->random(rand(1,3))->pluck('id'));
        }
        $this->call(GenreTableSeeder::class);
        $this->call(BookTableSeeder::class);
    }
}
