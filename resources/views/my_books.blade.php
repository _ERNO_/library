@extends('layouts.app')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Название книги</th>
                            <th>Статус возврата</th>
                            <th>Дата возврата</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach(\Illuminate\Support\Facades\Auth::user()->readers as $reader)
                    <tr>
                        <td>{{$reader->book}}</td>
                        <td>{{$reader->status}}</td>
                        <td>{{$reader->data}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
