@extends('layouts.app')

@section('content')
    <div class="row" style="display: flex; position: fixed; margin-left: 20px">
        <div class="col-2">
            <h3>Жанры:</h3><br>
            <h5><b> <a href="{{route('genres.index')}}">Все</a></b></h5>
            @foreach($genres as $g)
                <a  href="{{route('genres.show', ['genre' => $g])}}">
                    <h5><b style="color: forestgreen">{{$g->title}}</b></h5>
                </a>
            @endforeach
        </div>
    </div>
<div class="container">
    <hr>
    <div class="row">

        <div class="col-md-auto">
            <table class="table">
                <tbody>
                <tr>
                    @foreach($books as $book)
                        <td>
                            <div class="card" style="width: 18rem;">
                                <img src="{{'/storage/' . $book->picture}}" class="card-img-top" alt="{{$book->picture}}">
                                <div class="card-body">
                                    <h5 class="card-text">{{$book->author}}</h5>
                                    <h5 class="card-title">"{{$book->title}}"</h5>
                                    @if($book->data)
                                        <h5>{{$book->data}}</h5>
                                    @else
                                        <a href="{{action([\App\Http\Controllers\BookController::class, 'receiving_the_book'], ['book' => $book])}}" class="btn btn-primary"> Получить</a>
                                    @endif
                                </div>
                            </div>
                        </td>
                    @endforeach
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-auto">
            {{$books->links('pagination::bootstrap-4')}}
        </div>

    </div>
</div>

@endsection
