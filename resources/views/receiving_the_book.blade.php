@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
         <h4><b>Номер читательского билета: </b> {{session()->get('number')}}</h4>
            <h4><b>Id: </b>{{\Illuminate\Support\Facades\Auth::id()}}</h4>
            <h4><b>автор: </b>{{$book->author}}</h4>
            <h4><b>наименование книги: </b>{{$book->title}}</h4>
            <form action="{{action([\App\Http\Controllers\BookController::class, 'receiving_the_book'], ['book' => $book])}}" method="post">
                @csrf
                @method('put')

                <input type="date" name="data">
                <br><br>
                <button>
                    Подать заявку
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
