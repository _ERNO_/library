<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Genre;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class GenreController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $genres = Genre::all();
        $books = Book::paginate(4);
        return view('genres.index', compact('genres', 'books'));
    }


    public function show(Genre $genre)
    {
        $genres = Genre::all();
        $genre->setRelation('books', $genre->books()->paginate(4));
        return view('genres.show', compact('genre', 'genres'));
    }
}
