<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class MyBookController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('my_books');
    }
}
