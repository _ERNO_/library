<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }


    /**
     * @param Book $book
     * @return Application|Factory|View
     */
    public function receiving_the_book(Book $book)
    {
        return view('receiving_the_book', compact('book'));
    }

    /**
     * @param Book $book
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Book $book, Request $request): RedirectResponse
    {
        $book->data = $request->input('data');
        $book->update();
        return redirect()->route('genres.index');
    }
}
