<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Reader extends Model
{
    use HasFactory;

    public const EXPECTED = 'Expected';
    public const RETURNED = 'Returned';
    public const EXPIRED = 'Expired';

    public const STATUSES = [self::EXPIRED, self::RETURNED, self::EXPECTED];

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
